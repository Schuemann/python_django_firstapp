from django.urls import path

from . import views


app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),                                # polls
    path('hello/', views.other_site, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),            # polls/detaisl
    path('<int:question_id>/results/', views.results, name='results'),  # polls/results
    path('<int:question_id>/vote/', views.vote, name='vote')            # polls/vote
]