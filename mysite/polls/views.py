from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.http import Http404

from .models import Question


# Create your views here.
def index(request):
    latest_questions_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')  # get template
    context = {'latest_question_list': latest_questions_list}   # load content for template
    return render(request, 'polls/index.html', context)     # render template with content and return (shortcut)


def other_site(request):
    return HttpResponse("Hello, now you are somewhere else")


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/details.html', {'question': question})


def results(request, question_id):
    response = "You're looking at result %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You are voting on Question %s." % question_id)


